package com.franciscocalaca.aula02.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.aula02.model.Produto;

@Repository
public interface ProdutoDao extends JpaRepository<Produto, Integer>{

}
